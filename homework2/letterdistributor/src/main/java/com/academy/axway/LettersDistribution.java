package com.academy.axway;

import java.io.IOException;

public interface LettersDistribution {

    void doLettersDistribution(String directoryToSearch) throws IOException;
}
